package com.epam.fibonacci;
/**
 * import Arrey list
 */

import java.util.ArrayList;

/**
 * My class Fibo nember fibonacci
 */
class Fibo {
    private int beginOfRange;
    private int endOfRange;
    private int countOfFibonacciNumbers;
    /** Declaring three arrays for odd, even and Fibonacci numbers*/
    ArrayList<Integer> OddNumbers = new ArrayList<Integer>();
    ArrayList<Integer>EvenNumbers = new ArrayList<Integer>();
    ArrayList<Integer>FibonacciNumbers = new ArrayList<Integer>();

    /**
     * getBeginOfRange
     * @return beginOfRange
     */
    private int getBeginOfRange() {
        return beginOfRange;
    }

    /**
     * SetBeginOfRange
     * @param beginOfRange begin of Range
     */
    void setBeginOfRange(int beginOfRange) {
        this.beginOfRange = beginOfRange;
    }

    /**
     * getendOfRange
     * @return endOfRange
     */
    private int getEndOfRange() {
        return endOfRange;
    }

    /**
     *
     * @param endOfRange endOfRange
     */
    void setEndOfRange(int endOfRange) {
        this.endOfRange = endOfRange;
    }

    /**
     * setCountOfFibonacciNumbers
     * @param  countOfFibonacciNumbers countOfFibonacciNumbers
     */
    void setCountOfFibonacciNumbers(int countOfFibonacciNumbers) {
        this.countOfFibonacciNumbers = countOfFibonacciNumbers;
    }
    /** This method define all odd numbers from a given range*/
    void OddNumbers(){ //не парні
        for (int i = getBeginOfRange(); i<getEndOfRange();i++){
            if (i % 2 == 1)
                OddNumbers.add(i);
        }
        System.out.println("Odd numbers: " + OddNumbers);
    }
    /** This method define all even numbers from a given range*/
    void EvenNumbers(){ //парні
        for (int i = getEndOfRange(); i > getBeginOfRange();i--){
            if (i % 2 == 0)
                EvenNumbers.add(i);
        }
        System.out.println("Even numbers: " + EvenNumbers);
    }
    /** This method find the sum of odd numbers from a given range*/
    void SummOfOddNumbers(){ //сума непарних
        int summOfOddNumbers = 0;
        for (Integer i : OddNumbers){
            summOfOddNumbers += i;
        }
        System.out.println("Summ of odd numbers: " + summOfOddNumbers);
    }
    /** This method find the sum of even numbers from a given range*/
    void SummOfEvenNumbers(){ //сума парних
        int summOfEvenNumbers = 0;
        for (Integer i : EvenNumbers){
            summOfEvenNumbers += i;
        }
        System.out.println("Summ of even numbers: " + summOfEvenNumbers);
    }
    /** This method build the Fibonacci numbers*/
    void FibonacciNumbers(){        //будує ряд фібоначі
        FibonacciNumbers.add(OddNumbers.get(OddNumbers.size()-1));
        FibonacciNumbers.add(EvenNumbers.get(0));
        while(FibonacciNumbers.size()<countOfFibonacciNumbers) {
            int index = FibonacciNumbers.size();
            int numberFibonacci1 = FibonacciNumbers.get(index - 2);
            int numberFibonacci2 = FibonacciNumbers.get(index - 1);
            FibonacciNumbers.add(numberFibonacci1 + numberFibonacci2);
        }
        System.out.println("Fibonacci numbers: " + FibonacciNumbers);
    }
    /** This method find the percent of odd and even numbers from Fibonacci numbers*/
    void PercentageOfOddAndEvenFibonacciNumbers(){ //запис індексів парних та непарних чисел
        int countOfOddFibonacciNumbers = 0;
        int countOfEvenFibonacciNumbers = 0;
        for (Integer i:FibonacciNumbers){
            if (i % 2 == 0)
                countOfEvenFibonacciNumbers++;
            else countOfOddFibonacciNumbers++;
        }
        double percentageOfOddFibonacciNumbers, percentageOfEvenFibonacciNumbers; // процент кількості парних та непарних
        percentageOfEvenFibonacciNumbers = (double)countOfEvenFibonacciNumbers/FibonacciNumbers.size()*100;
        percentageOfOddFibonacciNumbers = 100 - percentageOfEvenFibonacciNumbers;
        System.out.println("Percentage of even Fibonacci numbers: " + percentageOfEvenFibonacciNumbers + "%");
        System.out.println("Percentage of odd Fibonacci numbers: " + percentageOfOddFibonacciNumbers + "%");
    }
}