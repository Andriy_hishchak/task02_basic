package com.epam.fibonacci;
import java.util.Scanner;

/**
 * Lab №1 "Fibonacci numbers"
 * @author Andriy Hischak, student of team34 group
 *
 * Main class in program
 */
public class Main {
    /** Main Starting program
     * @param args ,param String main
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Fibo numbers = new Fibo();
        System.out.print("Enter begin of range: ");
        numbers.setBeginOfRange(in.nextInt());
        System.out.print("Enter end of range: ");
        numbers.setEndOfRange(in.nextInt());
        System.out.print("Enter the count of Fibonacci numbers: ");
        numbers.setCountOfFibonacciNumbers(in.nextInt());

        numbers.OddNumbers();
        numbers.EvenNumbers();
        numbers.SummOfOddNumbers();
        numbers.SummOfEvenNumbers();
        numbers.FibonacciNumbers();
        numbers.PercentageOfOddAndEvenFibonacciNumbers();
    }
}

